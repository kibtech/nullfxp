// gccfeatures.h --- 
// 
// Author: liuguangzhao
// Copyright (C) 2007-2010 liuguangzhao@users.sf.net
// URL: 
// Created: 2009-10-18 09:07:05 +0800
// Version: $Id: gccfeatures.h 577 2009-11-21 07:35:43Z liuguangzhao $
// 

#ifndef _GCCFEATURES_H_
#define _GCCFEATURES_H_

// 6.1 Statements and Declarations in Expressions
void gcc_feature_statements_and_declarations_in_expressions();

#endif /* _GCCFEATURES_H_ */
