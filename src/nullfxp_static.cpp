// nullfxp_static.cpp --- 
// 
// Author: liuguangzhao
// Copyright (C) 2007-2010 liuguangzhao@users.sf.net
// URL: 
// Created: 2010-09-29 09:59:20 +0800
// Version: $Id: nullfxp_static.cpp 699 2010-09-29 07:01:46Z liuguangzhao $
// 

#include <QtCore>
#include <QtPlugin> 

Q_IMPORT_PLUGIN(qcncodecs)
Q_IMPORT_PLUGIN(qtwcodecs)
Q_IMPORT_PLUGIN(qkrcodecs)
Q_IMPORT_PLUGIN(qjpcodecs)

