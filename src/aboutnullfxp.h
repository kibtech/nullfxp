// aboutnullfxp.h --- 
// 
// Author: liuguangzhao
// Copyright (C) 2007-2010 liuguangzhao@users.sf.net
// URL: http://www.qtchina.net http://nullget.sourceforge.net
// Created: 2008-07-15 20:49:06 +0800
// Version: $Id: aboutnullfxp.h 729 2011-02-20 14:30:33Z liuguangzhao $
// 

#ifndef ABOUTNULLFXP_H
#define ABOUTNULLFXP_H

#include <QDialog>

namespace Ui {
    class AboutNullFXP;
};
/**
 * nullfxp关于信息对话框类
 * 
 * @author liuguangzhao
 */
class AboutNullFXP : public QDialog
{
    Q_OBJECT;
public:
    explicit AboutNullFXP(QWidget *parent = 0, Qt::WindowFlags f = 0);
    virtual ~AboutNullFXP();

    void dummyDepend();
private:
    Ui::AboutNullFXP *uiw;
};

#endif
